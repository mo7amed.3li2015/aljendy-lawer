
<?php 
	$pageArabic="الجندى | الرئيسية";
	$pageEnglish="Eljendy | Home";	
	$action = isset($_GET['action']) ? $_GET['action'] : 'arabic';
	include 'assets/includs/header.php';

?>

		<!-- slider-section - start
		================================================== -->
		<section id="slider-section" class="slider-section-ar clearfix">

			<!-- item - start -->
			<div class="item slider-bg1">
				<div class="overlay-black">
					<div class="container">
						<h1 class="mb-30">
							<?php echo $action("title")?>
						</h1>
						<h3 class="clr-orange mb-30">
							<?php echo $action("welcome")?>
						</h3>
						<div class="text-right">
							<a href="contact.php?action=<?php echo $action ?>" class="custom-btn bg-orange waves-light">
								<span><?php echo $action("contactBtn")?></span>
								<i class="ion-ios-arrow-back"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
			<!-- item - end -->

			<!-- item - start -->
			<div class="item slider-bg2">
				<div class="overlay-black">
					<div class="container">
						<h1 class="mb-30">
							<?php echo $action("title")?>
						</h1>
						<h3 class="clr-orange mb-30">
							<?php echo $action("welcome")?>
						</h3>
						<div class="text-right">
							<a href="contact.php?action=<?php echo $action ?>" class="custom-btn bg-orange waves-light">
								<span><?php echo $action("contactBtn")?></span>
								<i class="ion-ios-arrow-back"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
			<!-- item - end -->

			<!-- item - start -->
			<div class="item slider-bg3">
				<div class="overlay-black">
					<div class="container">
						<h1 class="mb-30">
							<?php echo $action("title")?>
						</h1>
						<h3 class="clr-orange mb-30">
							<?php echo $action("welcome")?>
						</h3>
						<div class="text-right">
							<a href="contact.php?action=<?php echo $action ?>" class="custom-btn bg-orange waves-light">
								<span><?php echo $action("contactBtn")?></span>
								<i class="ion-ios-arrow-back"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
			<!-- item - end -->

			<!-- item - start -->
			<div class="item slider-bg4">
				<div class="overlay-black">
					<div class="container">
						<h1 class="mb-30">
							<?php echo $action("title")?>
						</h1>
						<h3 class="clr-orange mb-30">
							<?php echo $action("welcome")?>
						</h3>
						<div class="text-right">
							<a href="contact.php?action=<?php echo $action ?>" class="custom-btn bg-orange waves-light">
								<span><?php echo $action("contactBtn")?></span>
								<i class="ion-ios-arrow-back"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
			<!-- item - end -->

		</section>
		<!-- slider-section - end
		================================================== -->





		<!-- about-section - start
		================================================== -->
		<section id="about-section" class="about-section sec-ptb-100 clearfix">
			<div class="container">
				<div class="row">

					<!-- about-us-area - start -->
					<div class="col-lg-6 col-md-12 col-sm-12">
						<div class="section-title">
							<h3><?php echo $action("aboutSection")?></h3>
							<h2>
								<?php echo $action("aboutTitle")?></h2>
						</div>

						<div class="about-us-area">
							<h4 class="title-small mb-10">
									<?php echo $action("title")?>
							</h4>
							<h5 class="title-xsmall mb-20">
									<?php echo $action("subTitle")?>
							</h5>
							<p class="mb-40"><?php echo $action("aboutUs")?></p>

							<a href="about.php?action=<?php echo $action ?>" class="custom-btn bg-blue waves-light waves-effect waves-light" tabindex="0">
								<span><?php echo $action("readMore")?></span>
								<i class="ion-ios-arrow-back"></i>
							</a>

						</div>
					</div>
					<!-- about-us-area - end -->

					<!-- question-area - start -->
					<div class="col-lg-6 col-md-12 col-sm-12">
						<img src="assets/images/about/aljendy-img.jpeg" alt="">
					</div>
					<!-- question-area - end -->

				</div>
			</div>
		</section>
		<!-- about-section - end
		================================================== -->



		<!-- features-section - start
		================================================== -->
		<section id="features-section" class="features-section clearfix">
			<div class="container">

				<!-- features-items-area - start -->
				<ul class="features-items-area z-depth-1">

					<li>
						<div class="feature-title mb-15">
							<h2>
								<i class="ion-android-checkmark-circle"></i>
								<?php echo $action("featureTile3")?>
							</h2>
						</div>

						<div class="feature-contant">

							<p><?php echo $action("featureContent3")?></p>
						</div>
					</li>

					<li>
						<div class="feature-title mb-15">
							<h2>
								<i class="ion-android-checkmark-circle"></i>
								<?php echo $action("featureTile2")?>
							</h2>
						</div>

						<div class="feature-contant">
							<p><p><?php echo $action("featureContent2")?></p></p>
						</div>
					</li>

					<li>
						<div class="feature-title mb-15">
							<h2>
								<i class="ion-android-checkmark-circle"></i>
								<?php echo $action("featureTile1")?>
							</h2>
						</div>

						<div class="feature-contant">

							<p><p><?php echo $action("featureContent1")?></p></p>
						</div>
					</li>
					
				</ul>
				<!-- features-items-area - end -->


				<!-- features-items-area-2 - start -->
				<ul class="features-items-area2 z-depth-1">

					<li>
						<div class="feature-title mb-15">
							<h2>
								<i class="ion-android-checkmark-circle"></i>
								رؤيتنا
							</h2>
						</div>

						<div class="feature-contant">
							<h3>1</h3>
							<p>
								الريادة والتميز في تقديم الحلول والخدمات القانونية المبتكرة للشركات التجارية والأفراد وفق أرقى المعايير المهنية والأخلاقية؛ بحيث نكون مكوِّنًا أساسيًّا في نجاح عملائنا.
							</p>
						</div>
					</li>

					<li>
						<div class="feature-title mb-15">
							<h2>
								<i class="ion-android-checkmark-circle"></i>
								رسالتنا
							</h2>
						</div>

						<div class="feature-contant">
							<h3>2</h3>
							<p>
								تطوير الخدمات القانونية المقدمة لعملائنا و تقديم الحلول المبتكرة لعملائنا بناءً على الدراسات الشرعية والقانونية و نشر الثقافة والتوعية القانونية من خلال مناشط المكتب.

							</p>
						</div>
					</li>
					
					<li>
						<div class="feature-title mb-15">
							<h2>
								<i class="ion-android-checkmark-circle"></i>
									مبادئنا
							</h2>
						</div>

						<div class="feature-contant">
							<h3>3</h3>
							<p>
								الجودة |
								الإلمام بالعمل |
								السرية والكتمان |
								الدقة والإتقان |
								السرعة في الأداء |
								تحمل المسؤولية.
							</p>
						</div>
					</li>
					
				</ul>
				<!-- features-items-area-2 - end -->


			</div>
		</section>
		<!-- features-section - end
		================================================== -->



		<!-- service-section - start
		================================================== -->
		<section id="service-section" class="service-section sec-ptb-100 clearfix">
			<div class="container">

				<div class="section-title">
					<h3><?php echo $action("serviceSubTitle")?></h3>
					<h2><?php echo $action("serviceTitle")?></h2>
				</div>


				<!-- service-slider - start -->

				<div class="row">
					<div class="col-12 col-md-4">
						<!-- item - start -->
						<a href="service-details-1.php?action=<?php echo $action ?>">
							<div class="item">
								<div class="overlay-black mb-30">
									<h4 class="title-xsmall mb-10"><?php echo $action("servicesTitle1")?></h4>
									<p><?php echo $action("servicesContent1")?></p>
								</div>
							</div>
						</a>
						<!-- item - end -->
					</div>
					<div class="col-12 col-md-4">
						<!-- item - start -->
						<a href="service-details-3.php?action=<?php echo $action ?>">
							<div class="item">
								<div class="overlay-black  mb-30">
									<h4 class="title-xsmall mb-10"><?php echo $action("servicesTitle3")?></h4>
									<p><?php echo $action("servicesContent3")?></p>
								</div>
							</div>
						</a>
						<!-- item - end -->
					</div>
					<div class="col-12 col-md-4">
						<!-- item - start -->
						<a href="service-details-2.php?action=<?php echo $action ?>">
							<div class="item">
								<div class="overlay-black mb-30">
									<h4 class="title-xsmall mb-10"><?php echo $action("servicesTitle2")?></h4>
									<p><?php echo $action("servicesContent2")?></p>
								</div>
							</div>
						</a>
						<!-- item - end -->
					</div>
					<div class="col-12 col-md-4">
						<!-- item - start -->
						<a href="service-details-4.php?action=<?php echo $action ?>">
							<div class="item">
								<div class="overlay-black  mb-30">
									<h4 class="title-xsmall mb-10"><?php echo $action("servicesTitle4")?></h4>
									<p><?php echo $action("servicesContent4")?></p>
								</div>
							</div>
						</a>
						<!-- item - end -->
					</div>
					<div class="col-12 col-md-4">
						<!-- item - start -->
						<a href="service-details-5.php?action=<?php echo $action ?>">
							<div class="item">
								<div class="overlay-black  mb-30">
									<h4 class="title-xsmall mb-10"><?php echo $action("servicesTitle5")?></h4>
									<p><?php echo $action("servicesContent5")?></p>
								</div>
							</div> 
						</a>
						<!-- item - end -->
					</div>
				</div>
				<!-- service-slider - end -->

			</div>
		</section>
		<!-- service-section - end
		================================================== -->

 

		<!-- team-section - start
		================================================== -->
		<section id="team-section" class="team-section sec-ptb-100 clearfix">
			<div class="container">

				<!-- section-title - start -->
				<div class="section-title text-center">
					<h2 class="mb-30"><?php echo $action("teamTitle")?></h2>
					<p><?php echo $action("teamContent")?></p>
				</div>
				<!-- section-title - end -->

				<div class="row d-flex justify-content-center">
						<!-- team - start -->
						<div class="col-lg-4 col-md-12 col-sm-12">
							<div class="team">
	
								<div class="team-img">
									<img src="assets/images/about/aljendy-img.jpeg" alt="image">
								</div>
	
								<div class="team-contant clearfix">
									<ul class="post-mate ul-li">
										<li>
											<a href="team-details-1.php?action=<?php echo $action ?>" class="clr-orange">
											    <h5><?php echo $action("aljendy")?></h5>
											</a>
										</li>
									</ul>
	
									<h3 class="title-xsmall mb-40"><?php echo $action("itemText1")?> </h3>
	
								</div>
								
							</div>
						</div>
						<!-- team - end -->
						<!-- team - start -->
						<div class="col-lg-4 col-md-12 col-sm-12">
							<div class="team">
	
								<div class="team-img">
									<img src="assets/images/about/fahad-img.jpg" alt="image">
								</div>
	
								<div class="team-contant clearfix">
									<ul class="post-mate ul-li">
										<li>
											<a href="team-details-2.php?action=<?php echo $action ?>" class="clr-orange">
												<h5><?php echo $action("fahad")?></h5>
											</a>
										</li>
									</ul>
	
									<h3 class="title-xsmall mb-40">  
										<?php echo $action("itemText2")?> 											
									</h3>
	
								
							</div>
						</div>
						<!-- team - end -->
					</div>
			</div>
		</section>
		<!-- team-section - end
		================================================== -->



		<!-- quote-section - start
		================================================== -->
		<section id="quote-section" class="quote-section sec-ptb-100 clearfix">

			<span class="bg-icon ion-paper-airplane"></span>

			<div class="container">
				<div class="row">

					<!-- quote-section-img - start -->
					<div class="col-lg-7 col-md-12 col-sm-12">
						<div class="quote-section-img">
							<img src="assets/images/contact/contact-img.jpg" alt="image">
						</div>
					</div>
					<!-- quote-section-img - end -->

					<!-- quote-form - start -->
					<div class="col-lg-5 col-md-12 col-sm-12">
						<form class="quote-form">

							<div class="section-title">
								<h3><?php echo $action("contactSubTitle")?></h3>
								<h2><?php echo $action("contactTitle")?></h2>
							</div>

							<div class="row">

								<div class="col-lg-6 col-md-6 col-sm-12">
									<input type="text" placeholder="<?php echo $action("name")?>">
								</div>

								<div class="col-lg-6 col-md-6 col-sm-12">
									<input type="email" placeholder="<?php echo $action("email")?>">
								</div>

								<div class="col-lg-6 col-md-6 col-sm-12">
									<input type="text" placeholder="<?php echo $action("phone")?>">
								</div>
								<div class="col-lg-6 col-md-6 col-sm-12">
									<input type="text" placeholder="<?php echo $action("subject")?>">
								</div>

								<div class="col-lg-12 col-md-12 col-sm-12">
									<textarea placeholder="<?php echo $action("consultationArea")?>"></textarea>
								</div>

								<div class="col-lg-12 col-md-12 col-sm-12">
									<a href="#!" class="custom-btn bg-orange waves-light waves-effect waves-light" tabindex="0">
										<span><?php echo $action("SendBtn")?></span>
										<i class="ion-android-send"></i>
									</a>
								</div>

							</div>
						</form>
					</div>
					<!-- quote-form - end -->

				</div>
				<!-- row - end -->
			</div>
			<!-- container - end -->

		</section>
		<!-- quote-section - end
		================================================== -->

<?php
	include 'assets/includs/footer.php';
?>