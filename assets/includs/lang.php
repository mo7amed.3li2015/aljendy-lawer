<?php


// here 2 function each function for lang , it's easy and all langs in one page.



function title($action , $english , $arabic){
    if($action =="english"){
        return $english;
    }else{
        return $arabic; 
    }
}


function arabic ($word){
   static $words = array(

            "lang"=>"العربية",
            "aljendy"=>"المدير العام الاستاذ /عبدالله بن عمر الجندي",
            "fahad"=>"نائب المدير الاستاذ /فهد بن صالح الدبيان",
            "title" => "مكتب/ عبد الله الجندي للمحاماة",
            "welcome" => "الريادة في تقديم الحلول القانونية في منازعات الشركات وقطاع الأعمال",
            "contactBtn" => "تواصل معنا",
            "aboutSection" => "الق نظرة" ,
            "aboutTitle" => "من نحن",
            "subTitle" => "(محامون - مستشارون - محكمون)",
            "aboutUs" => "منشأة قانونية تعمل على تقديم الحلول والاستشارات القانونية للشركات التجارية والأفراد، والقيام بأعمال التمثيل القانوني أمام المحاكم في درجات التقاضي المختلفة، وصياغة العقود المحلية والدولية، وخدمات الوساطة والتحكيم، وإجراء الأبحاث والدراسات القانونية، وغيرها من الخدمات. ",
            "aboutUsBig" => "منشأة قانونية تعمل على تقديم الحلول والاستشارات القانونية للشركات التجارية والأفراد، والقيام بأعمال التمثيل القانوني أمام المحاكم في درجات التقاضي المختلفة، وصياغة العقود المحلية والدولية، وخدمات الوساطة والتحكيم، وإجراء الأبحاث والدراسات القانونية، وغيرها من الخدمات. ويتم تقديم هذه الخدمات من خلال توظيف قدرات ومهارات نخبة من المستشارين والمحامين والباحثين أصحاب الكفاءة العالية والخبرة الواسعة؛ لتقديم الحلول والخدمات الاستشارية القانونية والشرعية وفقًا لأفضل معايير الجودة المهنية. ",
            "readMore" => "إقراء المزيد",
            "featureTile1" => "رؤيتنا",
            "featureTile2" => "رسالتنا",
            "featureTile3" => "مبادئنا ",
            "featureContent1" => "الريادة والتميز في تقديم الحلول والخدمات القانونية المبتكرة للشركات التجارية والأفراد وفق أرقى المعايير المهنية والأخلاقية؛ بحيث نكون مكوِّنًا أساسيًّا في نجاح عملائنا.",
            "featureContent2" => "تطوير الخدمات القانونية المقدمة لعملائنا و تقديم الحلول المبتكرة لعملائنا بناءً على الدراسات الشرعية والقانونية و نشر الثقافة والتوعية القانونية من خلال مناشط المكتب. ",
            "featureContent3" => "الجودة | الإلمام بالعمل | السرية والكتمان | الدقة والإتقان | السرعة في الأداء | تحمل المسؤولية. ",
            "serviceSubTitle" => "لدينا افضل ",
            "serviceTitle" => "الخدمات",
            "servicesContent1" => "نولي في مكتب عبد الله الجندي للمحاماة والاستشارات القانونية اهتماماً عالياً بتقديم الاستشارات الشرعية والقانونية للشركات التجارية والأفراد في كافة التخصصات...",
            "servicesContent2" => "نتميز بمكتب عبدالله الجندي للمحاماة والاستشارات القانونية بقدرتنا على تقديم الخدمات القانونية لعملائنا باحترافية من خلال تكوين فرق عمل متعددة ذات كفاءة عالية ومتخصصة",
            "servicesContent3" => "يقدم مكتب عبدالله الجندي للمحاماة والاستشارات القانونية الخدمات القانونية الخاصة بالشركات بمختلف أنواعها ومن ذلك: إجراءات تأسيس الشركات",
            "servicesContent4" => "نحن في مكتب عبدالله الجندي للمحاماة والاستشارات القانونية نسعى مع عملائنا لصياغة عقود مميز لعلاقة حقوقية مثمرة، كما نتولى تقديم خدمات الصياغة القانونية للجهات الحكومية والهيئات والشركات...",
            "servicesContent5" => "يعتبر التحكيم من أهم الوسائل المفضلة لحلّ النزاعات؛ وهو أحد الوسائل السريعة في حل النزاعات والمخاصمات خارج أروقة المحاكم؛ ولذا فإننا في مكتب عبدالله الجندي للمحاماة والاستشارات القانونية...",
            "servicesTitle1" => "الاستشارات الشرعية والقانونية",
            "servicesTitle2" => "التقاضي وتسوية المنازعات",
            "servicesTitle3" => "خدمات الشركات",
            "servicesTitle4" => "الصياغة القانونية والتشريعية",
            "servicesTitle5" => "التحكيم",
            "teamTitle" => "إدارة فريق العمل",
            "ourTeamTitle" => "فريق العمل",
            "teamContent" => "يباشر الاستاذ / عبدالله بن عمر الجندي ونائبه الأستاذ/ فهد بن صالح الدبيان الإشراف العام والمتابعة الدائمة لكافة الأعمال القانونية الموكلة لفريق العمل من المستشارين والمحامين والباحثين الشرعيين والقانونيين، بما يعزز من جودة مخرجات الأعمال القانونية، ويضمن تطابقها مع المعايير والواجبات المهنية.",
            "outTeam" => " يضم المكتب نخبةً من المحامين والمستشارين والباحثين الشرعيين والقانونيين أصحاب الكفاءة والخبرات العلمية والعملية من حملة الشهادات العليا، بما يتيح لهم تقديم الحلول بشكلها الأمثل والأنسب، وفقًا لموقف العميل من الناحية الشرعية والقانونية. ",
            "itemText1" => "بكالوريوس كلية الشريعة وأصول الدين بالقصيم، بتقدير ممتاز .",
            "itemText2" => "ماجستير في القانون التجاري الدولي من جامعة لاتروب في دولة أستراليا. ",
            "contactTitle" => "إستشارة",
            "contactSubTitle" => "طلب",
            "name" => "الأسم",
            "email" => "الإيميل",
            "phone" => "الجوال",
            "subject" => "الموضوع",
            "consultationArea" => "إستشارتك",
            "SendBtn" => "إرسل إستشارتك",
            "home" => "الرئيسية",
            "about" => "من نحن",
            "services" => "خدماتنا",
            "activities" => "أنشطة المكتب",
            "activitiesLinks1" => "اتفاقيات وشراكات عمل",
            "activitiesLinks2" => "ندوات ومؤتمرات",
            "activitiesLinks3" => "دورات تدريبية  ",
            "activitiesLinks4" => "لقاءات تلفزيونيه ",
            "organStructure" => "الهيكل التنظيمى",
            "contact" => "تواصل معنا",
            "quickLinks"=>["الرئيسية" , "من نحن" , "خدماتنا" , "أنشطة المكتب" , "الهيكل التنظيمى" , "تتواصل معنا"],
            "footerSer" => "خدماتنا",
            "footerLinks" => "روابط سريعة",
            "footerInfo" => "معلومات الأتصال",
            "address" => "المملكة العربية السعودية - الرياض - طريق الملك فهد - تقاطع طريق العروبة - برج توليب - رقم  (8592)",
            "postalCode" => "الرمز البريدي: 12333 ",
            "additionalCode" => "الرمز الإضافي: 3774 ",
            "phoneOffice" => "0556441766",
            "emailOffice" => "aljendy.law@gmail.com",
            "ourServices"=>["الاستشارات الشرعية والقانونية" , "التقاضي وتسوية المنازعات" , "خدمات الشركات" , "الصياغة القانونية والتشريعية" , "التحكيم" ],
            "serTitle1" => "الاستشارات الشرعية والقانونية",
            "serTitle2" => "التقاضي وتسوية المنازعات",
            "serTitle2-1" => "ويشمل نطاق الخدمات القانونية التي نقدمها في هذا المجال ",
            "serTitle3" => "خدمات الشركات",
            "serTitle4" => "الصياغة القانونية والتشريعية",
            "serTitle5" => "التحكيم",
            "serContent1" => "نولي في مكتب عبد الله الجندي للمحاماة والاستشارات القانونية اهتماماً عالياً بتقديم الاستشارات الشرعية والقانونية للشركات التجارية والأفراد في كافة التخصصات، بناءً على دراسة الوضع القانوني لعملائنا، وتقديم الحل الأمثل لهذا الوضع القائم، مع تقديم النصائح لتجنب الوقوع في المشكلات المشابهة مستقبلًا؛ مما يوفر أمانًا للعميل في الحاضر، ورؤية واضحة لخطواته في المستقبل. ",
            "serContent2" => ["	الترافع وتقديم اللوائح والمذكرات أمام جميع المحاكم بمختلف درجاتها، وكذلك اللجان القضائية كلجنة الفصل في منازعات الأوراق المالية، ولجنة المنازعات المصرفية، ولجان الفصل في المنازعات والمخالفات التأمينية، واللجان الزكوية والضريبية، واللجان الجمركية وغيرها." , "مباشرة الخدمات الإنهائية لدى المحاكم المختصة ومن ذلك (حجج الاستحكام، حصر الورثة، إثبات الوصايا والأوقاف) وغيرها من الإنهاءات. " , "	تقديم خدمات الحراسة القضائية وقسمة التركات وتصفية الشركات والكيانات العائلية. " , " تقديم الاعتراضات أمام محاكم الاستئناف والمحاكم العليا، والتماس إعادة النظر وغيرها مما هو من شؤون الترافع والتقاضي." , " المطالبة بتنفيذ الأحكام (المحلية والأجنبية) والقرارات الصادرة عن الجهات القضائية وهيئات التحكيم. "],
            "serContent3" => ["إجراءات تأسيس الشركات " , " التصفية الرضائية " , " إدارة الصفقات الخاصة. " , " حوكمة الشركات. " , "	إدارة عمليات الدمج والاستحواذ. " ,"		إعداد الدراسات النافية للجهالة.  " , "  تسجيل الوكالات والعلامات والأسماء التجارية.  ", "  تسييل الأصول والموجودات.", " 	تأسيس وحماية حقوق براءات الاختراع. ", " 	خدمات المساندة والتمثيل لرجال الأعمال والمنشآت في عمليات التفاوض وانتهاءً بإتمام أعمال الصفقات", "	التمثيل القانوني في الجمعيات العمومية العادية والغير عادية وإبداء الرأي القانوني فيما يتعلق بالإجراءات اللازمة لانعقادها. ",],
            "serContent4" => "نحن في مكتب عبدالله الجندي للمحاماة والاستشارات القانونية نسعى مع عملائنا لصياغة عقود مميز لعلاقة حقوقية مثمرة، كما نتولى تقديم خدمات الصياغة القانونية للجهات الحكومية والهيئات والشركات بسن الأنظمة واللوائح الداخلية ومراجعتها بشكل قانوني.",
            "serContent5" => "يعتبر التحكيم من أهم الوسائل المفضلة لحلّ النزاعات؛ وهو أحد الوسائل السريعة في حل النزاعات والمخاصمات خارج أروقة المحاكم؛ ولذا فإننا في مكتب عبدالله الجندي للمحاماة والاستشارات القانونية نولي ذلك عناية خاصة من خلال اختبار المحكمين المناسبين لطبيعة النزاع، وذلك ابتداءً من صياغة وثائق التحكيم، فتمثيل العملاء أمام هيئات التحكيم مروراً بالمشاركة في هيئات التحكيم وانتهاءً برئاستها. ",
            "allSer" => "جميع الخدمات",
            "serContent3-1" => " يقدم مكتب عبدالله الجندي للمحاماة والاستشارات القانونية الخدمات القانونية الخاصة بالشركات بمختلف أنواعها ومن ذلك",
            "serContent3-2" => " بالإضافة الى غيرها من الخدمات ذات الصلة مراعين في ذلك ما تتطلبه معايير السرعة والجودة في الأداء، وتأتي هذه الخدمات تزامنًا مع الأشكال الحديثة في وجوه الاستثمار المحلي والأجنبي الذي تشهده المملكة العربية السعودية في اقتصادها الذي يعيش نهضة متميزة تضاعفت على أثرها فرص الاستثمارات من قبل رجال الأعمال والشركات في جُل الأنشطة التجارية.",
            "serContent2-1" => "نتميز بمكتب عبدالله الجندي للمحاماة والاستشارات القانونية بقدرتنا على تقديم الخدمات القانونية لعملائنا باحترافية من خلال تكوين فرق عمل متعددة ذات كفاءة عالية ومتخصصة وقادرة على التعامل مع المسائل القانونية المعقدة، وتقديم حلول فعالة تلبي حاجة ومتطلبات عملائنا. ",
            "serContent2-2" => "كما يمتد نطاق خدماتنا ليشمل كل ما من شأنه أن يكفل تسوية النزاع بكافة الطرق البديلة للتسوية كالوساطة والتوفيق، الأمر الذي يخدم عملائنا، مسخرين في ذلك خبراتنا الواسعة، انطلاقاً من حرصنا على حماية مصالح عملائنا وتجنباً لإجراءات التقاضي وما ينتج عنه من نفقات وإطالة لأمد النزاع.",

            "education" => "المؤهلات العلمية",
            "experience" => "الخبرات العملية",
            "achievements" => "الناتج العلمى",
            "educationDetails1" => [" بكالوريوس كلية الشريعة وأصول الدين بالقصيم، بتقدير ممتاز مع مرتبة (الشرف الأولى). ", " دبلـوم عالي فوق الجامعي في القـانون من معهد الإدارة العامة بالريـاض، بتقدير ممتاز مع مرتبة (الشرف الأولى). ", " ماجستيـر من قسم (الفقه) في كلية الشريعة والدراسات الإسلامية بجامعة أم القرى، بتقدير ممتـاز مع (مرتبة الشرف). ", "	باحث دكتوراه في (عقود التحالف بين الشركات) بـكلية الشريعة في جامعة القصيم. ", " دارس في (قسم الاقتصاد) في كلية الاقتصاد والعلوم الإدارية بجامعة الإمام محمد بن سعود الإسلامية بالرياض. ",],
            "workDetails1" => [" قاض في الدوائر الإدارية بديوان المظالم. ", " قاض ، ورئيس دوائر جزائية في ديـوان المظالم. ", " قاض، ورئيس دوائر تجارية في وزارة العدل وديوان المظالم. ", "	قاض، ورئيس دوائر في المحاكم العامة بوازرة العدل.", "	عضـو مكتب الشؤون الفنية برئاسة ديوان المظالم، والمختص بإعداد الدراسات الشرعية والنظـامية. ", " عضو فريق تصنيف مدونـات الأحكـام القضـائية في ديوان المظالم. ", " عضـو فريق تصنيف مدونـات الأحكـام التجـارية للأعوام 1407هـ -1423هـ. ", " عضو لجنة المقابلات الوظيفية في ديوان المظالم. ", "	عضو المجلس الاستشاري بجامعة القصيم. ", " محكم دولي معتمد في المنازعات التجارية. ", " مدرب قانوني معتمد من المؤسسة العامة للتدريب التقني والمهني. ",],
            "achievementsDetails1" => ["	التعـارض بين النص العام والخاص، وأثره عند الأصوليين مع تطبيق ذلك على الأحكام القضـائية ", " تأثر القانون الغربي بالفقه الإسلامي (دراسة نقدية) ", " المنازعات التجارية ", "	الأعمال التجارية وتطبيقاتها القضائية. ", ],
            "educationDetails2" => [" بكالوريوس (شريعة) من جامعة الإمام محمد بن سعود الإسلامية", " ماجستير في القانون التجاري الدولي من جامعة لاتروب في دولة أستراليا.", ],
            "workDetails2" => ["محامي مرخص من وزارة العدل منذ عام 1432هـ.", "مستشار قانوني لدى عدد من الشركات السعودية الكبرى.", " أمين مجلس إدارة شركة أكوا باور (شركة مساهمة)", "أمين مجلس الإدارة، والمستشار القانوني والمشرف على إدارة الالتزام والشرعية في شركة سوليدرتي للتأمين التكافلي (شركة مساهمة عامة).", " مستشار قانوني في شركة علم لأمن المعلومات (شركة مساهمة مقفلة مملوكة للدولة).", " أمين مجلس الإدارة التأسيسي والمستشار القانوني لشركة تطوير لخدمات النقل المدرسي (شركة ذات مسؤولية محدودة مملوكة بالكامل للدولة.", " خبير متخصص في حوكمة الشركات وأمانة مجالس إداراتها ولجانها، وإدارة جمعيات المساهمين في الشركات المساهمة", " ممثل قانوني لعدد من الشركات الإقليمية والعالمية.",],
            "achievementsDetails2" => ["برنامج تدريب وتأهيل المحكمين في دول الخليج.", " برنامج القضاء التجاري.", "برنامج أنظمة المرافعات القضائية.",],
            "courses" => "الدورات والندوات",
            "workShare1" => "شارك في إعداد عدد من الأبحاث الفقهية والقضائية والقانونية والحقائب التدريبية المتخصصة، ومنها ",
            "workShare2" => "شارك في عدد من البرامج والدورات القضائية والقانونية المتخصصة، ومنها",
        
    
    );
    return $words[$word];
}



function english ($word){
    static  $words = array(

            "lang"=>"english",
            "aljendy"=>"Mr. Abdullah Bin Omar Aljendy",
            "fahad"=>"Mr. Fahad bin Saleh Aldebian",
            "title" => "Mr. Abdullah Aljendy Law Firm",
            "welcome" => "Leadership In Providing Legal Solutions In Corporate And Business Disputes",
            "contactBtn" => "Contact Us",
            "aboutSection" => "take a look",
            "aboutTitle" => "About Us",
            "subTitle" => "(Lawyers - Consultants - Arbitrators)",
            "aboutUs" => "A law firm that provides legal solutions and consultations for corporates and individuals, legal representation before courts in various levels of litigation, formulation of local and international contracts, mediation and arbitration services, legal researches and studies, and other services.",
            "aboutUsBig" => " Mr. Abdullah Bin Omar Aljendy Law Firm (Lawyers - Advisors - Arbitrators)
            A law firm that provides legal solutions and consultations for corporates and individuals, legal representation before courts in various levels of litigation, formulation of local and international contracts, mediation and arbitration services, legal researches and studies, and other services.
            These services are provided by employing the capacity and skills of a selection of highly qualified and experienced advisors, lawyers and researchers to provide the legal solutions and consulting services in accordance with the highest professional quality standards.",
            "readMore" => "Read More",
            "featureTile1" => "Our Vision",
            "featureTile2" => "Our Mission",
            "featureTile3" => "Our Goals",
            "featureContent1" => "Leading and excellence in providing innovative legal solutions and services in accordance with the highest professional and ethical standards so that we become a key component in the success of our clients.",
            "featureContent2" => "Provide legal protection to our clients in accordance with Shari'ah and the law. Develop the legal services provided to our clients. Provide the innovative solutions to our clients based on the legal studies. Disseminate the legal culture and awareness.",
            "featureContent3" => "Ongoing development. Advancing the legal services. Integration of the legal services.",
            "serviceSubTitle" => "We have the best",
            "serviceTitle" => "Services",
            "servicesContent1" => "Provide the legal advice to the corporates and individuals in all disciplines, based on studying the legal status of the client and...",
            "servicesContent2" => "Representation and pleading before the courts with their different degrees and jurisdiction, judicial authorities and quasi-judicial...",
            "servicesContent3" => "Provide the legal advice. Structure business according to the legal frameworks. Establish companies and business entities...",
            "servicesContent4" => "Negotiate, draft and review the commercial contracts in accordance with applicable laws and regulations. Provide the required legal...",
            "servicesContent5" => "Study the jurisprudence and legal principles. Study the laws and the international conventions. study the similar judicial precedents...",
            "servicesTitle1" => "Legal Advice",
            "servicesTitle2" => "Litigation and Dispute Resolution",
            "servicesTitle3" => "Corporate Services",
            "servicesTitle4" => "Commercial Contracts and Laws",
            "servicesTitle5" => "Legal Studies",
            "teamTitle" => "Team Management",
            "ourTeamTitle" => "Our Team",
            "teamContent" => "Mr. Abdullah Bin Omar Aljendy and his Deputy, Mr. Fahad bin Saleh Aldebian, shall undertake the general supervision and permanent follow-up of all legal work entrusted to the team of legal advisors, lawyers and researchers, in a manner that enhances the quality of the outputs of legal works and ensures their conformity with professional standards and duties.",
            "outTeam" => "Our law firm are consisting from a team represents a selection of legal advisors, lawyers and researchers, who are qualified and have scientific and practical experience along with higher academic degrees, allowing them to provide solutions in the most appropriate and optimal manner according to the legal and legitimate position of the client",
            "itemText1" => "Bachelor of the College of Sharia and the fundamentals of religion.",
            "itemText2" => "Master of International Commercial Law, La Trobe University in Australia. ",
            "contactTitle" => "Consultation",
            "contactSubTitle" => "Request",
            "name" => "Your Name",
            "email" => "Your Email",
            "phone" => "Your Phone",
            "subject" => "Subject",
            "consultationArea" => "Your Consultation",
            "SendBtn" => "Send Consultation",
            "home" => "Home",
            "about" => "About Us",
            "services" => "Our Services",
            "activities" => "Office Activities",
            "activitiesLinks1" => "Business",
            "activitiesLinks2" => "Seminars",
            "activitiesLinks3" => "Training ",
            "activitiesLinks4" => "TV Interviews",
            "organStructure" => "Organi Structure",
            "contact" => "Contact Us",
            "quickLinks"=>["Home" , "About Us" , "Our Services" , "Office Activities" , "Organi Structure" , "Contact Us"],
            "footerSer" => "Our Services",
            "footerLinks" => "Quick Links",
            "footerInfo" => "Contact Information",
            "address" => "KSA - Riyadh - King Fahd Road - Intersection of Al-Orouba Road - Tulip Tower - No. (8592)",
            "postalCode" => "Postal Code: 12333",
            "additionalCode" => "Additional Code: 3774",
            "phoneOffice" => "0556441766",
            "emailOffice" => "aljendy.law@gmail.com",
            "ourServices"=>["Legal Advice" , "Litigation and Dispute Resolution" , "Commercial Contracts and Laws" , "Legal Studies"],
            "serTitle1" => "Legal Advice",
            "serTitle2" => "Litigation and Dispute Resolution",
            "serTitle2-1" => " ",
            "serTitle3" => "Corporate Services",
            "serTitle4" => "Commercial Contracts and Laws",
            "serTitle5" => "Legal Studies",
            "serContent1" => "Provide the legal advice to the corporates and individuals in all disciplines, based on studying the legal status of the client and providing the optimal solution for this existing status along with the provision of advice to avoid similar problems in the future, providing security for the client in the present and a clear vision for its future steps. ",
            "serContent2" => "Representation and pleading before the courts with their different degrees and jurisdiction, judicial authorities and quasi-judicial bodies and committees, as well as submission of statements and memos, and preparation of statements of claim and statements of objection before judicial authorities and bodies.",
            "serContent3" => ["Provide the legal advice " , " Structure business according to the legal frameworks " , " Establish companies and business entities. " , "	Issue the required legal licenses for business " ,"		Prepare merger and acquisition agreements." , " Conduct due diligence legal examination.  ", " Register the trade names and marks and intellectual and industrial property rights.", " Provide the legal support on corporate governance and formulation of governance charters' rules. ", " 	Arrange and hold the companies’ general assemblies.", "	Liquidate companies and business entities.",],
            "serContent4" => "Negotiate, draft and review the commercial contracts in accordance with applicable laws and regulations.
            Provide the required legal advice for all the commercial transactions and contracts.
            Resolve the disputes of a commercial nature according to the professional principles and standards.",
            "serContent5" => "Study the jurisprudence and legal principles. | Study the laws and the international conventions. | study the similar judicial precedents and judgments. | Study the circulars by the related parties. | Extrapolate the previous practices of the Saudi judiciary. | Prepare and draft the legal studies.",
            "serContent5-0" => " ",
            "allSer" => "All Services",
            "serContent3-1" => " ",
            "serContent3-2" => " ",
            "serContent2-1" => " ",
            "serContent2-2" => " ",
            "education" => "Education",
            "experience" => "work experiences",
            "achievements" => "Achievements",
            "educationDetails1" => ["Bachelor of the College of Sharia and the fundamentals of religion in Qassim, excellent grade with (first-class honours).", "Postgraduate diploma in Law from the Institute of Public Administration in Riyadh, excellent grade with (first-class honours).", "Master degree (Department of Jurisprudence) in College of Shari`ah and Islamic Studies, Umm Al-Qura University, excellent grade with (first-class honours).", "PHD researcher in (consortium contracts) in Sharia College, Qassim University.", "Student at (Department of Economics), College of Economics and Administrative Sciences, Imam Muhammad Ibn Saud Islamic University in Riyadh.", ],
            "workDetails1" => ["Judge in the administrative departments of Board of Grievances.", "Judge and head of criminal departments in the Board of Grievances.","Judge and head of commercial departments in the Ministry of Justice Board of Grievances. ", "Judge and head of departments in the general courts in the Ministry of Justice.", "A member of the office of technical affairs in the office of President of the Board of Grievances which is in charge of the preparation of legal studies.", "A member of the team of judicial ruling codes classification in the Board of Grievances.", "A member of the team of judicial ruling codes classification of 1407 H - 1423 H. ", "A member of the Interviews Committee in the Board of Grievances.", "A member of the Advisory Board of Qassim University.", "A certified international arbitrator in the commercial disputes.", "A certified international arbitrator in the commercial disputes.", ],
            "achievementsDetails1" => ["He participated in the preparation of a number of jurisprudence, judicial and legal researches and the specialized training packages, including: ", "The Conflict Between General And Special Texts, And Its Impact With Fundamentalists While Applying the Same to Judicial Rulings", "Influence of Western Law By the Islamic Jurisprudence (Critical Study)", "Disputes of Partners in the Corporates.", "Commercial Disputes ", "Business And Its Judicial Applications.",],
            "educationDetails2" => ["Bachelor of College of Sharia, Imam Muhammad bin Saud Islamic University.", " Master of International Commercial Law, La Trobe University in Australia.",],
            "workDetails2" => ["A licensed lawyer by the Ministry of Justice since 1432 H.", "Legal advisor to a number of major Saudi companies.", "Secretary of the Board of Directors of ACWA Power (Joint-Stock Company)", "Secretary of the Board of Directors, Legal advisor and Supervisor of Compliance Department in Solidarity Saudi Takaful Company (Public Joint Stock Company).", "Legal advisor at Elm Information Security (Closed State-Owned Joint-Stock Company).", "Secretary of the Founding Board of Directors and Legal advisor of Tatweer Educational Transportation Services Company (Fully State-Owned Limited Company).", "Expert in corporate governance and secretariat of its boards of directors and committees, and management of shareholders’ assemblies in joint-stock companies", "Legal representative of a number of regional and international companies.",],
            "achievementsDetails2" => ["Arbitrator Training and Qualification Program in the Gulf States.", "Commercial Courts Program.", "Judicial Proceedings Law Program.",],
            
            "workShare1" => "He participated in the preparation of a number of jurisprudence, judicial and legal researches and the specialized training packages, including: ",
            "workShare2" => "He participated in a number of specialized judicial and legal programs and courses, including:",
        
            "courses" => "Courses and Seminars",
        );

    return $words[$word];
}


/*




*/


?>