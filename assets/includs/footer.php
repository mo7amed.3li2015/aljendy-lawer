
		<!-- footer-section - start
		================================================== -->
		<footer id="footer-section" class="footer-section clearfix">

			<!-- footer-top - start -->
			<div class="footer-top clearfix">
				<div class="overlay-black sec-ptb-100">
					<div class="container">
						<div class="row">

							<!-- ftr-top-contant - start -->
							<div class="col-lg-3 col-md-6 col-sm-12">
								<div class="ftr-top-contant">

									<?php
									if($action == "arabic"){
										?>
										
									<div class="text-right mb-40">
										<a href="index.php?action=<?php echo $action ?>" class="brand-logo">
											<img src="assets/images/brand-logo-ar.png" alt="Brand Logo">
										</a>
									</div>

										<?php

									}else{
										?>
									
									<div class="text-right mb-40">
										<a href="index.php?action=<?php echo $action ?>" class="brand-logo">
											<img src="assets/images/brand-logo-en.png" alt="Brand Logo">
										</a>
									</div>

										<?php
									}
									
									?>




									<ul class="social clearfix">

										<li>
											<a href="https://twitter.com/AljendyLawyer?s=08"  target="_blank" class="bg-twitter waves-light waves-effect waves-light">
												<i class="ion-social-twitter-outline"></i>
											</a>
										</li>
										<li>
											<a href="https://www.linkedin.com/in/%D9%85%D8%AC%D9%85%D9%88%D8%B9%D8%A9-%D8%B9%D8%A8%D8%AF%D8%A7%D9%84%D9%84%D9%87-%D8%A7%D9%84%D8%AC%D9%86%D8%AF%D9%8A-%D9%84%D9%84%D9%85%D8%AD%D8%A7%D9%85%D8%A7%D8%A9-a11737187/" target="_blank" class="bg-linkedin waves-light waves-effect waves-light">
												<i class="ion-social-linkedin-outline"></i>
											</a>
										</li>

									</ul>
								</div>
							</div>
							<!-- ftr-top-contant - end -->
							<!-- ftr-service-link - start -->
							<div class="col-lg-3 col-md-6 col-sm-12">
								<div class="ftr-service-link">

									<h2 class="title-small mb-20">
										<?php echo $action("footerSer")?>
									</h2>

									<ul class="ftr-link-list ul-li-block">
										<?php 
										$items = $action("ourServices");

										foreach ($items as $item) {
											?>
												
										<li>
											<a href="service-details-1.php?action=<?php echo $action ?>">
												<i class="ion-checkmark-circled"></i>
												<?php echo $item ?>
											</a>
										</li>
											<?php
										}

										?>
									</ul>

								</div>
							</div>
							<!-- ftr-service-link - end -->

							<!-- ftr-quick-link - start -->
							<div class="col-lg-3 col-md-6 col-sm-12">
								<div class="ftr-quick-link">

									<h2 class="title-small mb-20">
										<?php echo $action("footerLinks")?>
									</h2>

									<ul class="ftr-link-list ul-li-block">

										<?php

											$links = $action("quickLinks");
											foreach ($links as $link) {
												?>
												<li>
													<a href="index.php?action=<?php echo $action ?>">
														<i class="ion-checkmark-circled"></i>
														<?php echo $link?>
													</a>
												</li>
												<?php
											}

										?>


									</ul>

								</div>
							</div>
							<!-- ftr-quick-link - end -->

							<!-- ftr-location - start -->
							<div class="col-lg-3 col-md-6 col-sm-12">
								<div class="ftr-location">

									<h2 class="title-small mb-20">
										<?php echo $action("footerInfo")?>
									</h2>

									<ul class="ftr-link-list ul-li-block">
										<li>
										    <a href="https://www.google.com/maps/place/%D9%85%D8%AC%D9%85%D9%88%D8%B9%D8%A9+%D8%A7%D9%84%D8%AF%D9%83%D8%AA%D9%88%D8%B1+%D8%B9%D8%A8%D8%AF%D8%A7%D9%84%D9%84%D9%87+%D8%A7%D9%84%D8%AC%D9%86%D8%AF%D9%8A+%D9%84%D9%84%D9%85%D8%AD%D8%A7%D9%85%D8%A7%D8%A9%E2%80%AD/@24.7112497,46.669608,17z/data=!3m1!4b1!4m5!3m4!1s0x3e2f03c7daca3195:0x1c5eda82bf61e8b5!8m2!3d24.7112497!4d46.6717967" target="_blank">
    											<p class="mb-20">
    												<i class="ion-location"></i><?php echo $action("address")?></p>
    										</a>
											<p><?php echo $action("postalCode")?></p>
											<p><?php echo $action("additionalCode")?></p>
										</li>
										<li>
											<p>
												<i class="ion-iphone"></i>
												<?php echo $action("phoneOffice")?>
											</p>
										</li>
										<li>
											<p>
												<i class="ion-android-mail"></i>
												<?php echo $action("emailOffice")?>
											</p>
										</li>
									</ul>

								</div>
							</div>
							<!-- ftr-location - start -->

						</div>
					</div>
				</div>
			</div>
			<!-- footer-top - end -->



			<!-- footer-bottom - start -->
			<div class="footer-bottom clearfix" dir="ltr">
				<div class="container">
					<div class="row">

						<div class="col-lg-6 col-md-6 col-sm-12">
							<p class="copyright">
								Programming and Designed by 
								<a href="https://www.clicktopass.com/" target="blank" class="clr-orange"><u>clicktopass</u></a>
							</p>
						</div>

						<div class="col-lg-6 col-md-6 col-sm-12 text-right">
							<p>© Copyright, 2019 Aljendy For Lawyers and Consultants. All rights reserved.</p>
						</div>
						
					</div>
				</div>
			</div>
			<!-- footer-bottom - end -->

		</footer>
		<!-- footer-section - end
		================================================== -->












		<!-- mdb fraimwork - jquery include -->
		<script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="assets/js/popper.min.js"></script>
		<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="assets/js/mdb.min.js"></script>

		<!-- carousel jquery include -->
		<script type="text/javascript" src="assets/js/slick.min.js"></script>
		<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>

		
		<script src="http://maps.google.com/maps/api/js?key=AIzaSyC61_QVqt9LAhwFdlQmsNwi5aUJy9B2SyA"></script>
		<script type="text/javascript" src="assets/js/gmap3.min.js"></script>

		<!-- custom jquery include -->


		<?php
		if($action == "arabic"){
			?>
			<script type="text/javascript" src="assets/js/custom.js"></script>

			<?php

		}else{
			?>
		<script type="text/javascript" src="assets/js/customen.js"></script>

			<?php
		}
		
		?>


	</body>
</html>