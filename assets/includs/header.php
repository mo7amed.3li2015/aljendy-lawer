<?php
include 'back/dbcont.php';
include 'lang.php';


?>




<!DOCTYPE html>
<html lang="en" dir="rtl">

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">

		<title><?php echo title($action , $pageEnglish ,$pageArabic )  ?></title>
		<link rel="shortcut icon" href="assets/images/favicon.png">

		<!-- mdb fraimwork - css include -->
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/mdb.min.css">

		<!-- icon css include -->
		<link rel="stylesheet" type="text/css" href="assets/css/ionicons.css">
		<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">

		<!-- carousel css include -->
		<link rel="stylesheet" type="text/css" href="assets/css/slick.css">
		<link rel="stylesheet" type="text/css" href="assets/css/slick-theme.css">
		<link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.min.css">

		<!-- custom css include -->
		<link rel="stylesheet" type="text/css" href="assets/css/style.css">



		<?php
			
			if($action == 'arabic'){
				?>
				<link rel="stylesheet" type="text/css" href="assets/css/style.css">
				<?php
			}else{
				?>
				<link rel="stylesheet" type="text/css" href="assets/css/style-en.css">
				<?php
			}

			?>
		<link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
	</head>


	<body id="thetop" class="thetop">




		
		<!-- backtotop - start -->
		<div class='backtotop'>
			<a href="#" class='scroll'>
				<i class="ion-arrow-up-c"></i>
			</a>
		</div>
		<!-- backtotop - end -->

		<!-- preloader - start -->
		<div id="preloader"></div>
		<!-- preloader - end -->




		<!-- header-section - start
		================================================== -->
		<header id="header-section" class="header-section cd-auto-hide-header clearfix">

			<!-- header-top - start -->
			<div class="header-top">
				<div class="container">
					<div class="row">

						<!-- contact-info - start -->
						<div class="col-lg-8 col-md-8">
							<ul class="contact-info ul-li">


							<li>
									<i class="ion-android-phone-portrait"></i>
									0556441766
								</li>
								<li>
									<i class="ion-paper-airplane"></i>
									aljendy.law@gmail.com
								</li>
								<li>
									<i class="ion-clock"></i>
									Sun - Thu : 9.00 AM - 4.00 PM
								</li>

							</ul>
						</div>
						<!-- contact-info - end -->
						<!-- social - start -->
						<div class="col-lg-4 col-md-4">
							<ul class="social">

								<li>
									<a href="https://twitter.com/AljendyLawyer?s=08" class="bg-twitter waves-light">
										<i class="ion-social-twitter-outline"></i>
									</a>
								</li>
								<li>
									<a href="https://www.linkedin.com/in/%D9%85%D8%AC%D9%85%D9%88%D8%B9%D8%A9-%D8%B9%D8%A8%D8%AF%D8%A7%D9%84%D9%84%D9%87-%D8%A7%D9%84%D8%AC%D9%86%D8%AF%D9%8A-%D9%84%D9%84%D9%85%D8%AD%D8%A7%D9%85%D8%A7%D8%A9-a11737187/" class="bg-linkedin waves-light">
										<i class="ion-social-linkedin-outline"></i>
									</a>
								</li>

							</ul>
						</div>
						<!-- social - end -->

					</div>
				</div>
			</div>
			<!-- header-top - end -->



			<!-- header-bottom - start -->
			<div class="header-bottom">
				<div class="container">
					<div class="row">



						<?php
						if($action == "arabic"){
							?>
							<!-- brand-logo - start -->
							<div class="col-lg-2">
								<a href="index.php?action=<?php echo $action ?>" class="brand-logo">
									<img src="assets/images/brand-logo-ar.png" alt="Brand Logo">
								</a>
							</div>
							<!-- brand-logo - end -->

							<?php

						}else{
							?>
							<!-- brand-logo - start -->
							<div class="col-lg-2">
								<a href="index.php?action=<?php echo $action ?>" class="brand-logo">
									<img src="assets/images/brand-logo-en.png" alt="Brand Logo">
								</a>
							</div>
							<!-- brand-logo - end -->

							<?php
						}
						
						?>




						<!-- main-menu - start -->
						<div class="col-lg-8">
							<ul class="main-menu">
								<li class=""><a href="index.php?action=<?php echo $action ?>"><?php echo $action("home")?></a></li>
								<li><a href="about.php?action=<?php echo $action ?>"><?php echo $action("about")?></a></li>

								<li>
									<a href="#!"><?php echo $action("services")?> <i class="ion-arrow-down-b"></i></a>

									<div class="dropdown-area">
										<ul class="dropdown-list clearfix">
												<li><a href="service-details-1.php?action=<?php echo $action ?>"><?php echo $action("serTitle1")?></a></li>
												<li><a href="service-details-2.php?action=<?php echo $action ?>"><?php echo $action("serTitle2")?></a></li>
												<li><a href="service-details-3.php?action=<?php echo $action ?>"><?php echo $action("serTitle3")?></a></li>
												<li><a href="service-details-4.php?action=<?php echo $action ?>"><?php echo $action("serTitle4")?></a></li>
												<li><a href="service-details-5.php?action=<?php echo $action ?>"><?php echo $action("serTitle5")?></a></li>
										</ul>
									</div>
								</li>
								<li>
									<a href="#!">
										<?php echo $action("activities")?>
									    <i class="ion-arrow-down-b"></i>
									</a>
									<div class="dropdown-area">
										<ul class="dropdown-list clearfix">
												<li><a href="blog.php?action=1"><?php echo $action("activitiesLinks1")?></a></li>
												<li><a href="blog.php?action=2"><?php echo $action("activitiesLinks2")?></a></li>
												<li><a href="blog.php?action=3"><?php echo $action("activitiesLinks3")?></a></li>
												<li><a href="blog.php?action=4"><?php echo $action("activitiesLinks4")?> </a></li>
										</ul>
									</div>
								</li>
								<li>
									<a href="organ-structure.php?action=<?php echo $action ?>"><?php echo $action("organStructure")?></a>
								</li>
								<li>
									<a href="contact.php?action=<?php echo $action ?>"><?php echo $action("contact")?></a>
								</li>
							</ul>
						</div>
						<!-- main-menu - end -->

						<!-- quote-search-btn - start -->
						<div class="col-lg-2">
							<div class="quote-search-btn waves-light">
								<?php
									if($action == "english"){
										$action2 = "arabic";
									}else{
										$action2 = "english";
									}
								?>
								<a class="quote-btn"  href="<?php basename($_SERVER['PHP_SELF'])?>?action=<?php echo $action2 ?>"><?php echo $action2("lang")?></a>
								<!-- <a href="#!" class="quote-btn waves-light" data-toggle="modal" data-target="#quoteform">
									العربية
								</a>
								<a href="#!" class="quote-btn waves-light" data-toggle="modal" data-target="#quoteform">
									
									English
								</a> -->

								<!--<button type="button" class="toggle-overlay search-btn waves-light">
									<a href="index.php"> Eng </a>
								</button>-->

							</div>
						</div>
						<!-- quote-search-btn - end -->

					</div>
				</div>
			</div>
			<!-- header-bottom - end -->

		</header>
		<!-- header-section - end
		================================================== -->


		<!-- altranative navbar - start -->
		<nav class="alt-navbar navbar">

			<div style="overflow: hidden;">
				<!-- brand logo - start -->
				<a class="navbar-brand float-right" href="index.php?action=<?php echo $action ?>">
					<img src="assets/images/brand-logo-ar.png" alt="Brand Logo">
				</a>
				<!-- brand logo - end -->

				<!-- Collapse button - start -->
				<button class="alt-menu-btn navbar-toggler  float-left" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<i class="ion-android-menu"></i>
				</button>
				<!-- Collapse button - end -->
			</div>

			<!-- Collapsible content - start -->
			<div class="alt-menu collapse navbar-collapse mt-30" id="navbarSupportedContent">

				<!-- Links -->
				<ul class="alt-menu-list navbar-nav">

					<li class="nav-item ">
						<a class="nav-link" href="index.php?action=<?php echo $action ?>"><?php echo $action("home")?></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="about.php?action=<?php echo $action ?>"><?php echo $action("about")?> </a>
					</li>

					<li class="nav-item alt-dropdown-btn">
						<button type="button" class="nav-link" data-toggle="collapse" data-target="#service">
						<?php echo $action("services")?> <i class="ion-arrow-down-b"></i>
						</button>

						<ul id="service" class="alt-dropdown collapse">
								<li style="padding: 10px 25px; text-align: center;"><a href="service-details-1.php?action=<?php echo $action ?>"> <?php echo $action("serTitle1")?></a></li>
								<li style="padding: 10px 25px; text-align: center;"><a href="service-details-2.php?action=<?php echo $action ?>"><?php echo $action("serTitle2")?></a></li>
								<li style="padding: 10px 25px; text-align: center;"><a href="service-details-3.php?action=<?php echo $action ?>"><?php echo $action("serTitle3")?></a></li>
								<li style="padding: 10px 25px; text-align: center;"><a href="service-details-4.php?action=<?php echo $action ?>"><?php echo $action("serTitle4")?></a></li>
								<li style="padding: 10px 25px; text-align: center;"><a href="service-details-5.php?action=<?php echo $action ?>"><?php echo $action("serTitle5")?></a></li>
						</ul>
					</li>

					<li class="nav-item alt-dropdown-btn">
						<button type="button" class="nav-link" data-toggle="collapse" data-target="#activities">
						<?php echo $action("activities")?> <i class="ion-arrow-down-b"></i>
						</button>

						<ul id="activities" class="alt-dropdown collapse">
							<li style="padding: 10px 25px; text-align: center;">
								<a href="blog.php?action=1"><?php echo $action("activitiesLinks1")?></a>
							</li>
							<li style="padding: 10px 25px; text-align: center;">
								<a href="blog.php?action=2"><?php echo $action("activitiesLinks2")?></a>
							</li>
							<li style="padding: 10px 25px; text-align: center;">
								<a href="blog.php?action=3"><?php echo $action("activitiesLinks3")?> </a>
							</li>
							<li style="padding: 10px 25px; text-align: center;">
								<a href="blog.php?action=4"><?php echo $action("activitiesLinks4")?></a>
							</li>
						</ul>
					</li>

					
					<li class="nav-item">
						<a class="nav-link" href="organ-structure.php?action=<?php echo $action ?>"> <?php echo $action("organStructure")?></a>
					</li>

					<li class="nav-item">
						<a class="nav-link" href="contact.php?action=<?php echo $action ?>"> <?php echo $action("contact")?></a>
					</li>

					<!-- quote-search-btn - start -->
					<div class="col-lg-2">
						<div class="quote-search-btn waves-light">
							<?php
								if($action == "english"){
									$action2 = "arabic";
								}else{
									$action2 = "english";
								}
							?>
							<a class="quote-btn"  href="<?php basename($_SERVER['PHP_SELF'])?>?action=<?php echo $action2 ?>"><?php echo $action2("lang")?></a>
							<!-- <a href="#!" class="quote-btn waves-light" data-toggle="modal" data-target="#quoteform">
								العربية
							</a>
							<a href="#!" class="quote-btn waves-light" data-toggle="modal" data-target="#quoteform">
								
								English
							</a> -->

							<!--<button type="button" class="toggle-overlay search-btn waves-light">
								<a href="index.php"> Eng </a>
							</button>-->

						</div>
					</div>
					<!-- quote-search-btn - end -->
				</ul>
				<!-- Links -->

			</div>
			<!-- Collapsible content - end -->

		</nav>
		<!-- altranative navbar - end -->
 
