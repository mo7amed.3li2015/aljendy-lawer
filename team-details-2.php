

<?php 
	$pageArabic="الجندى | فريق العمل";
	$pageEnglish="Eljendy | Our Team";	
	$action = isset($_GET['action']) ? $_GET['action'] : 'arabic';
	include 'assets/includs/header.php';

?>


		<!-- breadcrumb-section - start
		================================================== -->
		<section id="breadcrumb-section" class="breadcrumb-section clearfix">
			<div class="overlay-black sec-ptb-100">
				<div class="container">

					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.php?action=<?php echo $action ?>"><?php echo $action("home")?></a></li>
						<li class="breadcrumb-item active"><?php echo $action("ourTeamTitle")?></li>
					</ol>

					<h2 class="breadcrumb-title">  
						<?php echo $action("fahad")?>
					</h2>

				</div>
			</div>
		</section>
		<!-- breadcrumb-section - end
		================================================== -->

		<!-- team-section - start
		================================================== -->
		<section id="team-section" class="team-section sec-ptb-100 clearfix">
			<div class="container">
				<div class="row">

					<!-- team-details - start -->
					<div class="col-lg-8 col-md-12 col-sm-12">
						<div class="team-details">

							<!-- team-details-img - start -->
							<div class="team-details-img mb-40">
								<img src="assets/images/about/fahad-img.jpg" alt="Image">
							</div>
							<!-- team-details-img - end -->


							<!-- team-details-contant - start -->
							<div class="team-details-contant mb-40">
								<h2 class="title-large mb-15">
									<?php echo $action("fahad")?>
								</h2>
								<h4 class="title-small mb-15"><?php echo $action("education")?></h4>
								

								<ul class="ftr-link-list ul-li-block">
										<?php

											$links = $action("educationDetails2");
											foreach ($links as $link) {
										?>
												<li>
													<i class="ion-checkmark-circled"></i>
													<?php echo $link?>
												</li>
										<?php
											}

										?>

								</ul>

								<h4 class="title-small mb-15"><?php echo $action("experience")?></h4>
								

								<ul class="ftr-link-list ul-li-block">
										<?php

											$links = $action("workDetails2");
											foreach ($links as $link) {
										?>
												<li>
													<i class="ion-checkmark-circled"></i>
													<?php echo $link?>
												</li>
										<?php
											}

										?>

								</ul>

								<h4 class="title-small mb-15"><?php echo $action("courses")?></h4>
								<p class="mb-20"><?php echo $action("workShare2")?></p>
								<ul class="ftr-link-list ul-li-block">
										<?php

											$links = $action("achievementsDetails2");
											foreach ($links as $link) {
										?>
												<li>
													<i class="ion-checkmark-circled"></i>
													<?php echo $link?>
												</li>
										<?php
											}

										?>

								</ul>
							</div>
							<!-- team-details-contant - end -->
						</div>

					</div>
					<!-- team-details - end -->

					<!-- right-sidebar - start -->
					<div class="col-lg-4 col-md-12 col-sm-12">
						<div class="right-sidebar">

							<!-- rs-team - start -->
							<div class="rs-team mb-30">
								<h2 class="title-small"><?php echo $action("ourTeamTitle")?></h2>
								<ul class="rs-team-list ul-li-block">
									<li>
										<a href="team-details-1.php?action=<?php echo $action ?>" class="waves-effect">
											<?php echo $action("aljendy")?>	
										</a>
									</li>
									<li>
										<a href="team-details-2.php?action=<?php echo $action ?>" class="waves-effect">
											<?php echo $action("fahad")?>
										</a>
									</li>
								</ul>
							</div>
							<!-- rs-team - end -->
							
						</div>
					</div>
					<!-- right-sidebar - end -->

				</div>
			</div>
		</section>
		<!-- team-section - end
		================================================== -->

<?php
	include 'assets/includs/footer.php';
?>

