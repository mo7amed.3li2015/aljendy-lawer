<?php 
	$pageArabic="الجندى | من نحن";
	$pageEnglish="Eljendy | About Us";	
	$action = isset($_GET['action']) ? $_GET['action'] : 'arabic';
	include 'assets/includs/header.php';

?>

		<!-- breadcrumb-section - start
		================================================== -->
		<section id="breadcrumb-section" class="breadcrumb-section clearfix">
			<div class="overlay-black sec-ptb-100">
				<div class="container">

					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.php"><?php echo $action("home")?></a></li>
						<li class="breadcrumb-item active"><?php echo $action("about")?></li>
					</ol>

					<h2 class="breadcrumb-title"><?php echo $action("about")?></h2>

				</div>
			</div>
		</section>
		<!-- breadcrumb-section - end
		================================================== -->





		<!-- about-section - start
		================================================== -->
		<section id="about-section" class="about-section sec-ptb-100 clearfix">
			<div class="container">

				<div class="row">
					<!-- about-us-area - start -->
					<div class="col-lg-6 col-md-12 col-xs-12">
						<h3 class="title-large mb-30 clr-orange"><?php echo $action("title")?></h3>

						<div class="about-us-area">
							<h4 class="title-small mb-40"><?php echo $action("subTitle")?></h4>

							<p class="mb-40"><?php echo $action("aboutUsBig")?></p>
						</div>
					</div>
					<!-- about-us-area - end -->

					<!-- question-area - start -->
					<div class="col-lg-6 col-md-12 col-xs-12">

						<ul class="slickslide slickslide-about">
							<li>
								<img src="assets/images/about/aljendy-img.jpeg" alt="image">
							</li>
						</ul>
					</div>
					<!-- question-area - end -->
				</div>


				<!-- more-about-us - start -->
				<div class="more-about-us">
					<div class="row">

						<!-- philosophy - start -->
						<div class="col-lg-4 col-md-12 col-sm-12">
							<div class="philosophy">
								<h3 class="title-large mb-40"><?php echo $action("featureTile1")?></h3>
								<p class="mb-15"><?php echo $action("featureContent1")?></p>
							</div>
						</div>
						<!-- philosophy - end -->

						<!-- mission - start -->
						<div class="col-lg-4 col-md-12 col-sm-12">
							<div class="mission">
								<h3 class="title-large mb-40"><?php echo $action("featureTile2")?></h3>
								<p class="mb-15"><?php echo $action("featureContent2")?></p>
								<p>
							</div>
						</div>
						<!-- mission - end -->

						<!-- vission - start -->
						<div class="col-lg-4 col-md-12 col-sm-12">
							<div class="vission">
								<h3 class="title-large mb-40"><?php echo $action("featureTile3")?></h3>
								<p class="mb-15"><?php echo $action("featureContent3")?></p>
							</div>
						</div>
						<!-- vission - end -->

					</div>
				</div>
				<!-- more-about-us - end -->

			</div>
		</section>
		<!-- about-section - end
		================================================== -->



		<!-- service-section - start
		================================================== -->
		<section id="service-section" class="service-section sec-ptb-100 clearfix">
			<div class="container">

				<div class="section-title">
					<h3><?php echo $action("serviceSubTitle")?></h3>
					<h2><?php echo $action("serviceTitle")?></h2>
				</div>



				<div class="row">

					<!-- item - start -->
					<div class="col-lg-4 col-md-12 col-sm-12">
						<a href="service-details-1.php">
							<div class="item">
								<div class="overlay-black mb-30">
									<h4 class="title-xsmall mb-10" ><?php echo $action("servicesTitle1")?></h4>
									<p><?php echo $action("servicesContent1")?></p>
								</div>
							</div>
						</a>
					</div>
					<!-- item - end -->
					<!-- item - start -->
					<div class="col-lg-4 col-md-12 col-sm-12">
						<a href="service-details-3.php">
							<div class="item">
								<div class="overlay-black mb-30">
									<h4 class="title-xsmall mb-10"><?php echo $action("servicesTitle3")?></h4>
									<p><?php echo $action("servicesContent3")?></p>
								</div>
							</div>
						</a>
					</div>
					<!-- item - end -->
					<!-- item - start -->
					<div class="col-lg-4 col-md-12 col-sm-12">
						<a href="service-details-2.php">
							<div class="item">
								<div class="overlay-black mb-30">
									<h4 class="title-xsmall mb-10"><?php echo $action("servicesTitle2")?></h4>
									<p><?php echo $action("servicesContent2")?></p>
								</div>
							</div>
						</a>
					</div>
					<!-- item - end -->
					<!-- item - start -->
					<div class="col-lg-4 col-md-12 col-sm-12">
						<a href="service-details-4.php">
							<div class="item">
								<div class="overlay-black mb-30">
									<h4 class="title-xsmall mb-10"><?php echo $action("servicesTitle4")?></h4>
									<p><?php echo $action("servicesContent4")?></p>
								</div>
							</div>
						</a>
					</div>
					<!-- item - end -->
					<!-- item - start -->
					<div class="col-lg-4 col-md-12 col-sm-12">
						<a href="service-details-5.php">
							<div class="item">
								<div class="overlay-black mb-30">
									<h4 class="title-xsmall mb-10"><?php echo $action("servicesTitle5")?></h4>
									<p><?php echo $action("servicesContent5")?></p>
								</div>
							</div>
						</a>
					</div>
					<!-- item - end -->
				</div>
				<!-- row - end -->

			</div>
		</section>
		<!-- service-section - end
		================================================== -->




		<!-- team-section - start
		================================================== -->
		<section id="team-section" class="team-section sec-ptb-100 clearfix">
				<div class="container">
	
					<!-- section-title - start -->
					<div class="section-title">
						<h2><?php echo $action("ourTeamTitle")?></h2>
					</div>
					<!-- section-title - end -->

					<!-- section-title - start -->
					<div class="section-title">
						<p><?php echo $action("outTeam")?></p>
					</div>
					<!-- section-title - end -->
					
										<!-- section-title - start -->
					<div class="section-title">
						<h2><?php echo $action("teamTitle")?></h2>
					</div>
					<!-- section-title - end -->

					<!-- section-title - start -->
					<div class="section-title">
						<?php echo $action("teamContent")?>
					</div>
					<!-- section-title - end -->
	
	
					<div class="row justify-content-md-center">
						<!-- team - start -->
						<div class="col-lg-4 col-md-12 col-sm-12">
							<div class="team">
	
								<div class="team-img">
									<img src="assets/images/about/aljendy-img.jpeg" alt="image">
								</div>
	
								<div class="team-contant clearfix">
									<ul class="post-mate ul-li">
										<li>
											<a href="team-details-1.php" class="clr-orange">
											    <h5><?php echo $action("aljendy")?></h5>
											</a>
										</li>
									</ul>
	
									<h3 class="title-xsmall"><?php echo $action("itemText1")?> </h3>
	
								</div>
								
							</div>
						</div>
						<!-- team - end -->
						<!-- team - start -->
						<div class="col-lg-4 col-md-12 col-sm-12">
							<div class="team">
	
								<div class="team-img">
									<img src="assets/images/about/fahad-img.jpg" alt="image">
								</div>
	
								<div class="team-contant clearfix">
									<ul class="post-mate ul-li">
										<li>
											<a href="team-details-2.php" class="clr-orange">
												<h5><?php echo $action("fahad")?></h5>
											</a>
										</li>
									</ul>
	
									<h3 class="title-xsmall">  
										<?php echo $action("itemText2")?> 											
									</h3>
								
							</div>
						</div>
						<!-- team - end -->
							
					</div>
				</div>
			</section>
			<!-- team-section - end
			================================================== -->

<?php
	include 'assets/includs/footer.php';
?>