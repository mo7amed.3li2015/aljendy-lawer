
<?php 
	$pageArabic="الجندى | الهيكل التنظيمى";
	$pageEnglish="Eljendy | Organ Structure";	
	$action = isset($_GET['action']) ? $_GET['action'] : 'arabic';
	include 'assets/includs/header.php';

?>
		<!-- breadcrumb-section - start
		================================================== -->
		<section id="breadcrumb-section" class="breadcrumb-section clearfix">
			<div class="overlay-black sec-ptb-100">
				<div class="container">

					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.php"><?php echo $action("home")?></a></li>
						<li class="breadcrumb-item active"> <?php echo $action("organStructure")?></li>
					</ol>

					<h2 class="breadcrumb-title"><?php echo $action("organStructure")?></h2>

				</div>
			</div>
		</section>
		<!-- breadcrumb-section - end
		================================================== -->





		<!-- service-section - start
		================================================== -->
		<section id="organ-structure" class="service-section sec-ptb-100 clearfix">
			<div class="container">
			    <embed src="organ-structure.pdf" type="application/pdf"   height="700px" width="100%">
			</div>
		</section>
		<!-- service-section - end
		================================================== -->

<?php
	include 'assets/includs/footer.php';
?>