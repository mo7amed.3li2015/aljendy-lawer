<?php 
	$pageArabic="الجندى | انشطة المكتب";
	$pageEnglish="Eljendy | Activities";	
	$action = isset($_GET['action']) ? $_GET['action'] : 'arabic';
	include 'assets/includs/header.php';

?>

		<!-- breadcrumb-section - start
		================================================== -->
		<section id="breadcrumb-section" class="breadcrumb-section clearfix">
			<div class="overlay-black sec-ptb-100">
				<div class="container">

					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.php?action=<?php echo $action ?>">الرئيسية</a></li>
						<li class="breadcrumb-item active">أنشطة المكتب</li>
					</ol>

					<h2 class="breadcrumb-title">اتفاقيات وشراكات عمل</h2>

				</div>
			</div>
		</section>
		<!-- breadcrumb-section - end
		================================================== -->





		<!-- service-section - start
		================================================== -->
		<section id="service-section" class="service-section sec-ptb-100 clearfix">
			<div class="container">
				<div class="row">

					<!-- service-details - start -->
					<div class="col-lg-8 col-md-12 col-sm-12">
						<div class="service-details">

							<!-- service-details-img - start -->
							<div class="service-details-img mb-40">
								<img src="assets/images/service/services-details-big.jpg" alt="Image">
							</div>
							<!-- service-details-img - end -->

							<!-- service-details-contant - start -->
							<div class="service-details-contant mb-40">
								<h2 class="title-large mb-15">
									الاستشارات الشرعية والقانونية
								</h2>

								<p class="mb-30">
									نولي في مكتب عبد الله الجندي للمحاماة والاستشارات القانونية اهتماماً عالياً بتقديم الاستشارات الشرعية والقانونية للشركات التجارية والأفراد في كافة التخصصات، بناءً على دراسة الوضع القانوني لعملائنا، وتقديم الحل الأمثل لهذا الوضع القائم، مع تقديم النصائح لتجنب الوقوع في المشكلات المشابهة مستقبلًا؛ مما يوفر أمانًا للعميل في الحاضر، ورؤية واضحة لخطواته في المستقبل.
								</p>
							</div>
							<!-- service-details-contant - end -->
							
						</div>
					</div>
					<!-- service-details - end -->

					<!-- right-sidebar - start -->
					<div class="col-lg-4 col-md-12 col-sm-12">
						<div class="right-sidebar">

							<!-- rs-services - start -->
							<div class="rs-services mb-30">
								<h2 class="title-small">
									جميع الأنشطة
								</h2>

								<ul class="rs-services-list ul-li-block">
									<li>
										<a href="activities-details-1.php?action=<?php echo $action ?>" class="waves-effect">
											<i class="ion-ios-checkmark-outline clr-orange"></i>
											اتفاقيات وشراكات عمل
										</a>
									</li>
									<li>
										<a href="activities-details-2.php?action=<?php echo $action ?>" class="waves-effect">
											<i class="ion-ios-checkmark-outline clr-orange"></i>
											ندوات ومؤتمرات
										</a>
									</li>
									<li>
										<a href="activities-details-3.php?action=<?php echo $action ?>" class="waves-effect">
											<i class="ion-ios-checkmark-outline clr-orange"></i>
											دورات تدريبية 
										</a>
									</li>
									<li>
										<a href="activities-details-4.php?action=<?php echo $action ?>" class="waves-effect">
											<i class="ion-ios-checkmark-outline clr-orange"></i>
								            لقاءات تلفزيونيه 
								    	</a>
									</li>
								</ul>
							</div>
							<!-- rs-services - end -->
							
						</div>
					</div>
					<!-- right-sidebar - end -->

				</div>
			</div>
		</section>
		<!-- service-section - end
		================================================== -->

<?php
	include 'assets/includs/footer.php';
?>