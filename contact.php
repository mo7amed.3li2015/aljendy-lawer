

<?php 
	$pageArabic="الجندى | تواصل معنا";
	$pageEnglish="Eljendy | Contact Us";	
	$action = isset($_GET['action']) ? $_GET['action'] : 'arabic';
	include 'assets/includs/header.php';

?>

		<!-- breadcrumb-section - start
		================================================== -->
		<section id="breadcrumb-section" class="breadcrumb-section clearfix">
			<div class="overlay-black sec-ptb-100">
				<div class="container">

					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.php?action=<?php echo $action ?>"><?php echo $action("home")?></a></li>
						<li class="breadcrumb-item active"><?php echo $action("contact")?></li>
					</ol>

					<h2 class="breadcrumb-title"><?php echo $action("contact")?></h2>

				</div>
			</div>
		</section>
		<!-- breadcrumb-section - end
		================================================== -->





		<!-- main-contact-section - start
		================================================== -->
		<section id="main-contact-section" class="main-contact-section sec-ptb-100 clearfix">

			<div class="container">
				<!-- section-title - start -->
				<div class="contact-section-title text-center">
					<h2><?php echo $action("contact")?></h2>

				</div>
				<!-- section-title - end -->
			</div>

			<!-- google-map - start -->
			<div class="main-google-map-wrapper">
				<div id="google-map">
					<div id="googleMaps" class="google-map-container"></div>
				</div>
			</div>
			<!-- google-map - end -->

			<div class="container">
				<div class="row">

					<!-- basic info - start -->
					<div class="col-lg-6 col-md-12 col-sm-12">
						<div class="basic-info">

							<div class="basic-info-item">
								<span class="icon">
									<i class="ion-ios-location"></i>
								</span>

								<p>
									<span><?php echo $action("address")?></span><br>
								</p>
							</div>

							<div class="basic-info-item">
								<span class="icon">
									<i class="ion-android-call"></i>
								</span>

								<p>
									<span>0556441766</span><br>
									<span>Sun - Thu : 9.00 AM - 4.00 PM</span>
								</p>
							</div>

							<div class="basic-info-item">
								<span class="icon">
									<i class="ion-ios-email"></i>
								</span>

								<p>
									<span>
										<?php echo $action("emailOffice")?>
									</span>

								</p>
							</div>

						</div>
					</div>
					<!-- basic info - end -->

					<!-- contact-form - start -->
					<div class="col-lg-6 col-md-12 col-sm-12">
						<form class="contact-form" action="#">

							<div class="row">

								<div class="col-lg-6 col-md-6 col-sm-12">
									<input type="text" placeholder="<?php echo $action("name")?>">
								</div>
								<div class="col-lg-6 col-md-6 col-sm-12">
									<input type="email" placeholder="<?php echo $action("email")?>">
								</div>

								<div class="col-lg-6 col-md-6 col-sm-12">
									<input type="text" placeholder="<?php echo $action("phone")?>">
								</div>
								<div class="col-lg-6 col-md-6 col-sm-12">
									<input type="text" placeholder="<?php echo $action("subject")?>">
								</div>

								<div class="col-lg-12 col-md-12 col-sm-12">
									<textarea placeholder="<?php echo $action("consultationArea")?>"></textarea>
								</div>

								<div class="col-lg-12 col-md-12 col-sm-12">
									<a href="#!" class="custom-btn bg-orange waves-light waves-effect waves-light" tabindex="0">
										<span><?php echo $action("SendBtn")?></span>
										<i class="ion-android-send"></i>
									</a>
								</div>

							</div>
						</form>
					</div>
					<!-- contact-form - end -->

				</div>
			</div>

		</section>
		<!-- main-contact-section - end
		================================================== -->



<?php
	include 'assets/includs/footer.php';
?>