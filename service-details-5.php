<?php 
	$pageArabic="الجندى | خدماتنا";
	$pageEnglish="Eljendy | Our Services";	
	$action = isset($_GET['action']) ? $_GET['action'] : 'arabic';
	include 'assets/includs/header.php';

?>

		<!-- breadcrumb-section - start
		================================================== -->
		<section id="breadcrumb-section" class="breadcrumb-section clearfix">
			<div class="overlay-black sec-ptb-100">
				<div class="container">

					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.php?action=<?php echo $action ?>"><?php echo $action("home")?></a></li>
						<li class="breadcrumb-item active"><?php echo $action("serviceTitle")?></li>
					</ol>

					<h2 class="breadcrumb-title"><?php echo $action("serTitle5")?></h2>

				</div>
			</div>
		</section>
		<!-- breadcrumb-section - end
		================================================== -->





		<!-- service-section - start
		================================================== -->
		<section id="service-section" class="service-section sec-ptb-100 clearfix">
			<div class="container">
				<div class="row">

					<!-- service-details - start -->
					<div class="col-lg-8 col-md-12 col-sm-12">
						<div class="service-details">

							<!-- service-details-img - start -->
							<div class="service-details-img mb-40">
								<img src="assets/images/service/services-details-big.jpg" alt="Image">
							</div>
							<!-- service-details-img - end -->

							<!-- service-details-contant - start -->
							<div class="service-details-contant mb-40">
								<h2 class="title-large mb-15"><?php echo $action("serTitle5")?></h2>

								<p class="mb-30"><?php echo $action("serContent5")?></p>

							</div>
							<!-- service-details-contant - end -->
							
						</div>
					</div>
					<!-- service-details - end -->

					<!-- right-sidebar - start -->
					<div class="col-lg-4 col-md-12 col-sm-12">
						<div class="right-sidebar">

							<!-- rs-services - start -->
							<div class="rs-services mb-30">
								<h2 class="title-small"><?php echo $action("allSer")?></h2>

								<ul class="rs-services-list ul-li-block">
									<li>
										<a href="service-details-1.php?action=<?php echo $action ?>" class="waves-effect">
											<i class="ion-ios-checkmark-outline clr-orange"></i>
											<?php echo $action("serTitle1")?></a>
										</a>
									</li>
									<li>
										<a href="service-details-2.php?action=<?php echo $action ?>" class="waves-effect">
											<i class="ion-ios-checkmark-outline clr-orange"></i>
											<?php echo $action("serTitle2")?></a>
										</a>
									</li>
									<li>
										<a href="service-details-3.php?action=<?php echo $action ?>" class="waves-effect">
											<i class="ion-ios-checkmark-outline clr-orange"></i>
											<?php echo $action("serTitle3")?></a>
										</a>
									</li>
									<li>
										<a href="service-details-4.php?action=<?php echo $action ?>" class="waves-effect">
											<i class="ion-ios-checkmark-outline clr-orange"></i>
											<?php echo $action("serTitle4")?></a>
										</a>
									</li>
									<li>
										<a href="service-details-5.php?action=<?php echo $action ?>" class="waves-effect">
											<i class="ion-ios-checkmark-outline clr-orange"></i>
											<?php echo $action("serTitle5")?></a>
										</a>
									</li>
								</ul>
							</div>
							<!-- rs-services - end -->

							
						</div>
					</div>
					<!-- right-sidebar - end -->

				</div>
			</div>
		</section>
		<!-- service-section - end
		================================================== -->

<?php
	include 'assets/includs/footer.php';
?>

