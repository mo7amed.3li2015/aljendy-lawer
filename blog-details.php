<?php 
	$pageArabic="الجندى | انشطة المكتب";
	$pageEnglish="Eljendy | Activities";	
	$action = isset($_GET['action']) ? $_GET['action'] : 'arabic';
	include 'assets/includs/header.php';

?>


		<!-- breadcrumb-section - start
		================================================== -->
		<section id="breadcrumb-section" class="breadcrumb-section clearfix">
			<div class="overlay-black sec-ptb-100">
				<div class="container">

					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.php?action=<?php echo $action ?>">الرئيسية</a></li>
						<li class="breadcrumb-item active">أنشطة المكتب</li>
					</ol>

					<h2 class="breadcrumb-title">أسم المقال</h2>

				</div>
			</div>
		</section>
		<!-- breadcrumb-section - end
		================================================== -->





		<!-- blog-section - start
		================================================== -->
		<section id="blog-section" class="blog-section sec-ptb-100 clearfix">
			<div class="container">
				<div class="row">

					<!-- blog-details - start -->
					<div class="col-lg-8 col-md-12 col-sm-12">
						<div class="blog-details">

							<!-- blog-details-img - start -->
							<div class="blog-details-img mb-40">
								<img src="assets/images/service/services-details-big.jpg" alt="Image">
							</div>
							<!-- blog-details-img - end -->

							<ul class="post-mate ul-li mb-40">
								<li>
									التاريخ: <span class="clr-black">6 Aug, 2019</span>
								</li>
							</ul>

							<!-- blog-details-contant - start -->
							<div class="blog-details-contant mb-40">
								<h2 class="title-large mb-15">
									عنوان المقال
								</h2>

								<p class="mb-30">
								هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.
								</p>

								<p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.</p>
							</div>
							<!-- blog-details-contant - end -->
						</div>

					</div>
					<!-- blog-details - end -->

					<!-- right-sidebar - start -->
					<div class="col-lg-4 col-md-12 col-sm-12">
						<div class="right-sidebar">
							<!-- rs-recent-post - start -->
							<div class="rs-recent-post mb-30">
								<h2 class="title-small">
									المنشورات الأخيرة
								</h2>

								<ul class="rs-recent-post-list ul-li-block">

									<li>
										<a href="blog-details.php?action=<?php echo $action ?>" class="waves-effect">
											<span class="post-img float-left">
												<img src="assets/images/blog/post1.jpg" alt="recent blog image">
											</span>
											Lorهذا النص هو مثال لنص يمكن أن يستبدل
											<small>6 Aug, 2019</small>
										</a>
									</li>
									<li>
										<a href="blog-details.php?action=<?php echo $action ?>" class="waves-effect">
											<span class="post-img float-left">
												<img src="assets/images/blog/post1.jpg" alt="recent blog image">
											</span>
											هذا النص هو مثال لنص يمكن أن يستبدل
											<small>6 Aug, 2019</small>
										</a>
									</li>
									<li>
										<a href="blog-details.php?action=<?php echo $action ?>" class="waves-effect">
											<span class="post-img float-left">
												<img src="assets/images/blog/post1.jpg" alt="recent blog image">
											</span>
											هذا النص هو مثال لنص يمكن أن يستبدل
											<small>6 Aug, 2019</small>
										</a>
									</li>
									<li>
										<a href="blog-details.php?action=<?php echo $action ?>" class="waves-effect">
											<span class="post-img float-left">
												<img src="assets/images/blog/post1.jpg" alt="recent blog image">
											</span>
											هذا النص هو مثال لنص يمكن أن يستبدل
											<small>6 Aug, 2019</small>
										</a>
									</li>

								</ul>
							</div>
							<!-- rs-recent-post - end -->
						</div>
					</div>
					<!-- right-sidebar - end -->

				</div>
			</div>
		</section>
		<!-- blog-section - end
		================================================== -->

<?php
	include 'assets/includs/footer.php';
?>